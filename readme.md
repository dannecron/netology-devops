Репозиторий создан для сохранения различных данных по курсу [netology DevOps-инженер](https://netology.ru/programs/devops).

## Homeworks

* [1.1 Введение в DevOps](/src/homework/1.1) 
* [2.1 Системы контроля версий](/src/homework/2.1)